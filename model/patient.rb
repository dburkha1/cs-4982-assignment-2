##
# Holds information about each patient
##
class Patient

  ##
  # read ability given to name and id
  ##
  attr_reader :name, :id

  ##
  # read and write ability given to weight
  ##
  attr_accessor :weight

  ##
  # The initialize method for a patient
  # @param [Object] id
  #                the id of the patient
  # @param [Object] name
  #                the name of the patient
  # @param [Object] weight
  #                the weight of the patient
  # @Precondition: id and name must both be valid objects
  # @Postcondition: a new patient is made
  ##
  def initialize(id, name, weight=nil)
    @id = id
    @name = name
    @weight = weight
  end

  ##
  # Overrides the To string method of Object.rb
  # @Precondition: none
  # @Postcondition: none
  ##
  def to_s
    "Patient: #{@id}--#{name} (#{weight})"
  end

  ##
  # function to determine if patient is overweight or not
  # @return if the patient is overweight or not
  # @Precondition: none
  # @Postcondition: none
  ##
  def is_overweight?
    if @weight > 300
      return 'Overweight'
    end

    return 'not overweight'

  end

end