load 'model/patient.rb'

##
# Stores information of patients in a
# collection
##
class Hospital

  ##
  # initializes instance variable to new array
  # @Precondition: none
  # @Postcondition: none
  ##
  def initialize
    @patients = Array.new
  end

  ##
  # Adds a patient to the list of patients
  # @param [Patient] patient the patient
  # @Precondition: none
  # @Postcondition: the patient is added to the list
  ##
  def add(patient)
    @patients.push(patient)
  end

  ##
  # Finds the patient in the list and returns the patient if found.
  # return negative 1 otherwise
  # @param [Object] patient
  #        The patient in question
  # @Precondition: none
  # @Postcondition: none
  ##
  def find(patient)
    @patients.each do |currPatient|
      return currPatient.index if currPatient.equal?(patient)
    end
    return -1
  end

  ##
  # Deletes the patient passed in to the method from the list
  # @param [Object] patient
  #      The patient passed in to be deleted
  # Precondition: the patient must be in the list
  # Postcondition: the patient is removed from the list
  def delete(patient)
    @patients.delete(patient)
  end

  ##
  # Determines the size of the list and returns it
  # @Precondition: none
  # @Postcondition: none
  ##
  def size
    return @patients.count
  end

  ##
  # Sorts the list by the ID of each patient
  # @Precondition: none
  # @Postcondition: a list sorted by ID
  ##
  def sort
    @patients = @patients.sort_by(&:id)
    puts @patients
  end

  ##
  # Sorts the list descending by name
  # @Precondition: none
  # @Postcondition: a list sorted by its name in descending order
  ##
  def sort_descending_by_name
    @patients = @patients.sort_by(&:name).reverse
    puts @patients
  end

  ##
  # sorts the list by weight
  # @Preconditon: none
  # @Postcondition: the list is sorted by weight
  ##
  def sort_weight
    @patients = @patients.sort_by(&:weight)
    puts @patients
  end

  ##
  # Override of the to string method for this class.
  # @Preconditon: none
  # @Postcondition: none
  ##
  def to_s
    @patients.each do |currPatient|
      puts currPatient.to_s
    end

  end

end

