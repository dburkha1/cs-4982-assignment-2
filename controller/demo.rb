load 'model/hospital.rb'
load 'model/patient.rb'

##
# Creates and tests instances of patient
# and hospital methods
#
class Demo

  patient_aaron = Patient.new(100, 'Aaron', 300)
  patient_brian = Patient.new(101, 'Brian', 150)
  patient_john = Patient.new(102, 'John', 350)
  patient_adam = Patient.new(103, 'Adam', 180)

  patient_aaron.weight = 500

  puts patient_aaron.is_overweight?

  hospital = Hospital.new

  hospital.add(patient_aaron)
  hospital.add(patient_brian)
  hospital.add(patient_john)

  puts hospital.find(patient_adam)

  puts "Sort descending by name"
  hospital.sort_descending_by_name
  puts "\nSort by id"
  hospital.sort
  puts "\nSort by weight"
  hospital.sort_weight


end